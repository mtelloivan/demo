package demo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@org.springframework.stereotype.Service
@Transactional
public class Service {

    @PersistenceContext
    private EntityManager em;

    public Persona crear(String nombre, String apellido, String dni, String telefono){
        Persona persona = new Persona();
        persona.setNombre(nombre);
        persona.setApellido(apellido);
        persona.setDni(dni);
        persona.setTelefono(telefono);
        em.persist(persona);
        return persona;
    }

    public Persona consultar(int id){
        Persona persona = em.find(Persona.class, id);
        return persona;
    }

}
