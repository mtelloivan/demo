package demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    private Service miService;

    @RequestMapping(path = "/crear")
    @ResponseBody
    public Persona crear(String nombre, String apellido, String dni, String telefono) {
        return miService.crear(nombre, apellido, dni, telefono);
    }

    @RequestMapping(path = "/consultar")
    @ResponseBody
    public Persona consultar(int id) {
        return miService.consultar(id);
    }

}
